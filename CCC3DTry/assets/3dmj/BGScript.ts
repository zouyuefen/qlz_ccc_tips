// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

import Utils from './Utils';

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bgNode: cc.Node = null;

    @property(cc.Camera)
    bgCamera: cc.Node = null;

    @property(cc.Camera)
    camera3d: cc.Node = null;

    onLoad(){
        Utils.resize();
        //this.fitBg();
        this.fit3D();
    }

    fitBg(){
        var dr = Utils.curDR;
        var frameSize = cc.view.getFrameSize();

        var drRatio = dr.width / dr.height;
        var realRatio = frameSize.width / frameSize.height;
        
        var diff = 0;

        //如果更宽，则需要将背景撑满
        if(realRatio > drRatio){
            this.bgNode.scaleX = realRatio / drRatio; 
        }
        else{
            //调整相机的 orthoSize
            var oldOrthSize = this.bgCamera.orthoSize;
            this.bgCamera.orthoSize = this.bgCamera.orthoSize / (realRatio / drRatio);

            diff = this.bgCamera.orthoSize - oldOrthSize;
        }        
        this.bgNode.y = 12 - diff;
    }

    fit3D(){

        var dr = Utils.curDR;
        var frameSize = cc.view.getFrameSize();

        var drRatio = dr.width / dr.height;
        var realRatio = frameSize.width / frameSize.height;
        //如果更高，则需要调节
        if(realRatio < drRatio){
            //this.camera3d.fov = this.camera3d.fov / (Math.atan(realRatio)/Math.atan(drRatio));
        }
    }

    start () {

    }

    // update (dt) {}
}
